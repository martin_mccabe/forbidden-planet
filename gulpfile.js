'use strict'

// by MartinM

// css
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

// browser-sync
var browserSync = require ('browser-sync');
var reload = browserSync.reload;

// js
var browserify = require('browserify');
var watchify = require('watchify');
var babelify = require('babelify');
var gutil = require('gulp-util');
var notify = require('gulp-notify');
var source = require('vinyl-source-stream');

// project options
var project = {
    entries: ['index.js'], // add all js files here that you need to be bundled
    js_src: './src/js/',
    js_output: './build/js/',
    common_js: 'app.js'
} 

//----------------------------------------------------------------------------------- gulp tasks 

/**
 * Images
 */
gulp.task('images', function(){
   gulp.src('./src/images/**')
   .pipe(gulp.dest('./build/images')) 
});

/**
 * Styles
 */
gulp.task('styles', function(){
    
    // first copy fonts
    gulp.src('./src/fonts/**')
    .pipe(gulp.dest('./build/fonts'));
    
    // compile css
    gulp.src(['./src/scss/**'])
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./build/css'))
    .pipe(reload({stream:true}));
});

/**
 * displays errors
 */
function handleErrors() {
  var args = Array.prototype.slice.call(arguments);
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>'
  }).apply(this, args);
  this.emit('end'); // Keep gulp from hanging on this task
}

/**
 * Javascript
 * I prefer this way as all common js is in one js file, all page-specific code in their own respective files.
 * Use the project options at the top of this document to specify entries, folders, etc.
 */
gulp.task('js', bundle);

function bundle(){
    function getEntries(){
        var e = [];
        var i;
        for (i=0; i<project.entries.length; i++)
        {
            e.push(project.js_src + project.entries[i]);
        }
        return e;
    }
    
    var extensions = ['.js','.json','.es6'];
    var props = {
        entries:getEntries(),
        transform: [babelify.configure({extensions: extensions, presets: ["es2015"]})]
    };
    
    var bundler = watchify(browserify(props));
    bundler.on('update', function(){
        rebundle();
        gutil.log('Rebundle...');
    });
    
    function rebundle(){
        bundler.bundle()
        .on('error', handleErrors)
        .pipe(source(project.common_js))
        .pipe(gulp.dest(project.js_output))
        .pipe(reload({stream:true}));
    }
    
    return rebundle();
}

/**
 * start browsersync
 */
gulp.task('serve', function(){ 
    browserSync({
        server: true,
        port: 8080,
        open: true,
        startPath: './build/'
    });
});

/**
 * HTML
 */
gulp.task('html', function(){
    gulp.src(['./src/**.html'])
    // do some magic here
    .pipe(gulp.dest('./build'));
})

/**
 * default task
 */
gulp.task('default', ['images','styles','js', 'html', 'serve'], function(){
    gulp.watch(['./src/scss/**'], ['styles']);
    gulp.watch(['./src/**.html'], ['html']);
    gulp.watch(['./src/images', './src/images/**'], ['images']);
    
});