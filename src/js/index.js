import {log, str} from './_helpers';
import Platform from './_platform';
import Robot from './_robot';
import TweenMax from 'gsap';
// import TimelineMax from 'gsap';

var game = document.getElementById("game");

var platform = new Platform(game);
var robot = new Robot(platform);

document.getElementById('command').addEventListener('keyup', function(e){
   if(e.keyCode == 13){
       runCommand(e.target.value, e.target);
       return false;
   } 
});

document.addEventListener("output", function (e){
    output(e.detail.message);
});
// document.dispatchEvent(new CustomEvent("output", {detail: {message: "hello from custom event!"}}));

// set starting point
TweenMax.set(platform.getElement(), {autoAlpha: 0});
TweenMax.set('.gamename', {autoAlpha:0, y: -100});
TweenMax.set('.commands', {autoAlpha:1, x: 400});
TweenMax.set('.ctrlbutton', {autoAlpha:0, y: 30});
TweenMax.set('.robot', {autoAlpha:0});
var tl = new TimelineMax();
    tl.to('.wrapper', 0.3, {opacity: 1})
    .to('.gamename', 0.5, {autoAlpha:1, y:0, delay: 0.5})
    .to('.gamename', 0.5, {x: 317, scaleX:0.4, scaleY: 0.4, y: -100, delay: 2 })
    .to(['.platform', '.robot'], 0.5, {autoAlpha:1})
    .to('.commands', 0.5, {autoAlpha:1, x: 0})
    .staggerTo(document.getElementsByClassName('ctrlbutton'), 0.3, {autoAlpha: 1, y: 0}, 0.1);


/**
 * run commands typed into input field
 */
function runCommand(command, input){
    // place command
    if(command.toLowerCase().indexOf('place') == 0){
        var params = command.substr(command.indexOf(' ')+1, command.length);
        params = params.replace(/\s+/g, '');
        params = params.split(',');
        if(robot.place(parseInt(params[0]), parseInt(params[1]), params[2])){
            output(('&gt; '+command).toUpperCase());
            input.value = '';
        }
    }
    
    // move command
    if(command.toLowerCase() == "move"){
        robot.move();
        output(('&gt; '+command).toUpperCase());
        input.value = '';
    }
    
    // turn left/right command
    if(command.toLowerCase() == 'left' || command.toLowerCase() == 'right'){
        robot.turn(command.toLowerCase());
        output(('&gt; '+command).toUpperCase());
        input.value = '';
    }
    
    // report command
    if(command.toLowerCase() == 'report'){
        output(('&gt; '+command).toUpperCase());
        robot.report();
        input.value = '';
    }
}

/**
 * display messages in output
 */
function output(message){
    var html = document.getElementById('output').innerHTML;
    var out = document.getElementById('output');
    out.innerHTML = html + message + '<br/>';
    out.scrollTop = out.scrollHeight;
}

//button actions
document.getElementById('btn-place').addEventListener('click', function (e){
    e.preventDefault();
    robot.place(2, 2, robot.NORTH);
    output ('&gt; '+'PLACE 2, 2, NORTH');
});

document.getElementById('btn-move').addEventListener('click', function (e){
    e.preventDefault();
    robot.move();
    output ('&gt; '+'MOVE');
});

document.getElementById('btn-left').addEventListener('click', function (e){
    e.preventDefault();
    robot.turn('left');
    output ('&gt; '+'LEFT');
});

document.getElementById('btn-right').addEventListener('click', function (e){
    e.preventDefault();
    robot.turn('right');
    output ('&gt; '+'RIGHT');
});

document.getElementById('btn-report').addEventListener('click', function (e){
    e.preventDefault();
    output ('&gt; '+'REPORT');
    robot.report();
});

document.getElementById('btn-toggle').addEventListener('click', function (e){
    e.preventDefault();
    output ('&gt; '+'TOGGLE GRID');
    platform.toggleGrid();
});