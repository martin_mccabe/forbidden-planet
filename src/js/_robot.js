/**
 * the Robot
 * @author MartinM
 **/
import {log, str} from './_helpers';
import TweenMax from 'gsap';

/**
 * Robot
 * @param platform the parent object/element where the robot will be placed
 */
export default function Robot(platform){
   
    // constants
    var _this = this;
    this.NORTH = 'north';
    this.SOUTH = 'south';
    this.WEST = 'west';
    this.EAST = 'east';
    this.LEFT = 'left';
    this.RIGHT = 'right';
    
    //properties
    var element;
    var _placed = false;
    var _xpos, _ypos, _dir, _currentDeg = 0;
    
    element = document.createElement('div');
    element.classList.add('robot');
    element.classList.add('not-placed');
    platform.getElement().appendChild(element);
    
    TweenMax.set(element, {x: platform.getStartPoint().x + platform.getWidth() + 100, y: 70, transformOrigin: "50% 50%", onComplete:function(){
        _shout("hello robot");
    }});
    
    
    // methods
    
    /**
     * show
     * shows the robot if hidden. Can only be called after place
     */
    this.show = function(){
        element.classList.remove('hidden');
    } 
    
    /**
     * hide
     * hides the robot. Can only be called after place
     */
    this.hide = function(){
        element.classList.add('hidden');
    }
    
    /**
     * place
     * place the robot on platform. Must be called before any other method
     * @param p object - properties must be x (x-coordinate), y(y-coordinate), f (direction)
     */
    this.place = function(x, y, f){
        var startPoint = platform.getStartPoint();
        var platformGridXItems = platform.getXitems();
        var platformGridYItems = platform.getYitems();
        var gridItemWidth = platform.getGridItemWidth();
        var gridItemHeight = platform.getGridItemHeight();
        
        var canMove = true;
        
        x -= 1;
        y -= 1;
        
        // check x
        if(x >=0 && x < platformGridXItems){
            // _xpos = x;
        }else{
            canMove = false;
            _shout('robot > place > place value (x) not inside grid area');
        }
        
        // check y
        if(y >= 0 && y < platformGridYItems){
            // _ypos = y;
        }else{
            canMove = false;
           _shout('robot > place > place value (y) not inside grid area');
        }
        
        // check direction
        if(f == _this.SOUTH || f == _this.NORTH || f == _this.WEST || f == _this.EAST){
            // _dir = f;
        }else{
            log(f)
            log(_this.NORTH)
            canMove = false;
            _shout('robot > place > What direction should I face?');
        }
        
        if(canMove){
            element.classList.remove('not-placed');
            _placed = true;
            _moveTo(x, y, f);
            // _this.show();
            return true;
        }
    }
    
    /**
     * MOVE command
     */
    this.move = function(){
        log('robot > move: dir: ' +_dir)
        if(_placed){
            switch (_dir){
                case _this.NORTH:
                    if(_checkCanMove(_xpos, _ypos+1)){
                        _moveTo(_xpos, _ypos+1, _dir);
                    }
                break;
                case _this.EAST:
                    if(_checkCanMove(_xpos+1, _ypos)){
                        _moveTo(_xpos+1, _ypos, _dir);
                    }
                break;
                case _this.SOUTH:
                    if(_checkCanMove(_xpos, _ypos-1)){
                        _moveTo(_xpos, _ypos-1, _dir);
                    }
                break;
                case _this.WEST:
                    if(_checkCanMove(_xpos-1, _ypos)){
                        _moveTo(_xpos-1, _ypos, _dir);
                    }
                break;
            }
        }
    }
    
    /**
     * turn robot to face to its left or right
     * @f string direction to face in (left or right)
     */
    this.turn = function(turnto){
        if(_placed){
            switch (turnto) {
                case _this.LEFT:
                    _moveTo(_xpos, _ypos, _getLeft());
                    break;
                case _this.RIGHT:
                    _moveTo(_xpos, _ypos, _getRight());
                    break;
            
                default:
                    break;
            }
        }
    }
    
    /**
     * report
     * reports with x, y coorinates and direction it's facing
     */
    this.report = function(){
        
        if(_placed){
            var result = 'OUTPUT: ' + (_xpos + 1) + ', ' + (_ypos + 1) + ', ' + _dir.toUpperCase();
            document.dispatchEvent(new CustomEvent("output", {detail: {message: result}}));
        }
    }
    
    /**
     * check if robot can move to desired coordinates
     */
    function _checkCanMove(x, y){
        if(x<0 || x > platform.getXitems()-1 || y<0 || y > platform.getYitems()-1){
            return false;
        }
        return true;
    }
    
    /**
     * move robot to pixels according to _xpos, _ypos
     */
    function _moveTo(x, y, f, callback){
        var x_pixels = platform.getStartPoint().x + (platform.getGridItemWidth() * x) - (element.offsetWidth * 0.5);
        var y_pixels = platform.getStartPoint().y - (platform.getGridItemHeight() * y) - (element.offsetHeight * 0.5);
        var rotateTo;
        if(f!=_dir){
            rotateTo = _getRotateDegrees(f);
        }else{
            rotateTo = _currentDeg;
        }
        _xpos = x;
        _ypos = y;
        _dir = f;
        TweenMax.to(element, 0.3, {x: x_pixels, y: y_pixels, rotationZ: rotateTo, transformOrigin: "50% 50%", onComplete:function(){
            _currentDeg = rotateTo;
            if(callback) callback();
        }});
    }
    
    /**
     * get direction that's to the LEFT of Robot
     */
    function _getLeft(){
        switch (_dir){
            case _this.NORTH:
                return _this.WEST;
            case _this.EAST:
                return _this.NORTH;
            case _this.SOUTH:
                return _this.EAST;
            case _this.WEST:
                return _this.SOUTH;
        }
    }
    
    /**
     * get direction that's to the RIGHT of Robot
     */
    function _getRight(){
        switch (_dir){
            case _this.NORTH:
                return _this.EAST;
            case _this.EAST:
                return _this.SOUTH;
            case _this.SOUTH:
                return _this.WEST;
            case _this.WEST:
                return _this.NORTH;
        }
    }
    
    /**
     * get degrees to rotate to for direction
     * @param dir string direction robot wants to face
     */
    function _getRotateDegrees(dir){
        var deg = 0;
        if(dir == _this.NORTH){
            if(_currentDeg == 270){
                TweenMax.set(element, {rotationZ: -90, transformOrigin: "50% 50%"});
            }
            deg = 0;
        }
        if(dir == _this.EAST){
            deg = 90;
        }
        if(dir == _this.SOUTH){
            deg = 180;
        }
        if(dir == _this.WEST){
            if(_currentDeg == 0){
                TweenMax.set(element, {rotationZ: 360, transformOrigin: "50% 50%"});
            }
            deg = 270;
        }
        return deg;
    }
    
    /**
     * shout a message
     */
    function _shout(message){
        log(message);
    }
    
    return this;
}