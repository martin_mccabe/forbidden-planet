/**
 * quick console.log, only writes to console if it's available - if you don't do this it breaks some stuff in IE.
 */
export function log(str){
    if(console){
        console.log(str);
    }
}

/**
 * stringify an object
 */
export function str(object){
    return JSON.stringify(object);
}