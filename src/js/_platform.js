/**
 * the Platform
 * @author MartinM
 */
import {log} from './_helpers';

/**
 * Platform
 * @param parent the element where table must be placed
 */
export default function Platform(parent, width, height, xItems, yItems, padding){
    var _element, _startPoint, _width, _height, _xItems, _yItems, _padding;
    
    if (parent == undefined){
        throw "Platform > Parent not specified";
    }
    
    _width = width || 500;
    _height = height || 500;
    _xItems = xItems || 5;
    _yItems = yItems || 5;
    _padding = padding || (_width/_xItems)/2;
    _element = document.createElement('div');
    _element.classList.add('platform');
    _element.style.width = (_width + 2*_padding)+'px';
    _element.style.height = (_height + 2*_padding)+'px';
    
    _startPoint = {
        x: _padding,
        y: _padding + _height
    }
    
    parent.appendChild(_element);
    
    // methods, getters, setters
    this.getElement = function(){
      return _element;
    }
    
    this.getWidth = function(){
        return _width;
    }
    
    this.getHeight = function(){
        return _height;
    }
    
    this.getXitems = function(){
        return _xItems;
    }
    
    this.getYitems = function(){
        return _yItems;
    }
    
    this.getStartPoint = function(){ 
        return _startPoint;
    }
    
    this.getGridItemWidth = function(){
        return _width / (_xItems-1);
    }
    
    this.getGridItemHeight = function(){
        return _height/(_yItems-1);
    }
    
    var _gridOn = false;
    var gridHolder = document.createElement('div');
    gridHolder.classList.add('gridHolder');   
    gridHolder.classList.add('hidden');   
    _element.appendChild(gridHolder); 
    for (var i=0; i< this.getYitems(); i++){
        var hr = document.createElement('div');
        hr.classList.add('platform-hr');
        gridHolder.appendChild(hr);
        TweenMax.set(hr, {y: this.getStartPoint().y - (i * this.getGridItemHeight()) });   
    }
    for (var j=0; j < this.getXitems(); j++){
        var vr = document.createElement('div');
        vr.classList.add('platform-vr');
        gridHolder.appendChild(vr);
        TweenMax.set(vr, {x: this.getStartPoint().x + (j * this.getGridItemWidth()) });   
    }
    
    this.toggleGrid = function(){
       _gridOn = !_gridOn;
       if(_gridOn){
           gridHolder.classList.remove('hidden');
       }else{
           gridHolder.classList.add('hidden');
       }
    }
    
    return this;
}